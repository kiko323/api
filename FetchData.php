<?php
  require_once 'Config.php';
  
  class FetchData {
    
  
    private function fetchFromFile () {
      
  
      $arrayOfData = [
    
        'JAN' => ["Google Analytics" => 432, "Positive Guys" => 7774],
        'FEB' => ["Google Analytics" => 466, "Positive Guys" => 8989],
        'MAR' => ["Google Analytics" => 890, "Positive Guys" => 8485],
      ];
  
      return $arrayOfData;
    }
    
    private function fetchFromDb () {
     // As long as the data from the DB is fetched and structured in
    // the same way as the other method's arrays are, there will be no problem.
    }
    
    private function fetchFromArray () {
  
      $arrayOfData = [
    
        'JAN' => ["Google Analytics" => 120, "Positive Guys" => 600],
        'FEB' => ["Google Analytics" => 340, "Positive Guys" => 530],
        'MAR' => ["Google Analytics" => 150, "Positive Guys" => 5000],
        'APR' => ["Google Analytics" => 333, "Positive Guys" => 3321],
        'MAY' => ["Google Analytics" => 323, "Positive Guys" => 3232],
        'JUN' => ["Google Analytics" => 981, "Positive Guys" => 6042],
        'JUL' => ["Google Analytics" => 980, "Positive Guys" => 4211],
        'AUG' => ["Google Analytics" => 765, "Positive Guys" => 5432],
        'SEP' => ["Google Analytics" => 567, "Positive Guys" => 1223],
        'OCT' => ["Google Analytics" => 900, "Positive Guys" => 9876],
        'NOV' => ["Google Analytics" => 833, "Positive Guys" => 8686],
        'DEC' => ["Google Analytics" => 136, "Positive Guys" => 4543],
      ];
  
      return $arrayOfData;
    }
    
    public function prepareForApi () {
      return $this->prepareArray($this->fetchFromArray(), $this->fetchFromFile());
    }
  
    private function prepareArray($FromArray = [], $fromFile = [], $fromDb = [])
    {
      $data = func_get_args();
      $finalArr = [];
      foreach ($data as $key => $datum) {
        foreach ($datum as $keyDatum => $value) {
          $finalArr [$keyDatum][] = $value;
        }
      }
      return $finalArr;
    }
    
    
    
  }
