<?php
  require_once 'Config.php';
  
  $configuration = new Config();
  
  ?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
<form action="" method="post">
  
  <div class="row">
    <div class="col-12">
      <div class="form-group">
        <label for="sel1">Select month:</label>
        <select class="form-control" name="<?php echo Config::TYPE_OF_DATA; ?>" id="sel1">
          <?php
            $dataForSelect = $configuration->dataForSelect();
            foreach ($dataForSelect as $item) {
              echo "<option> {$item} </option>";
            }
          ?>
        </select>
      </div>
    </div>
    <div class="col-12">
      <input type="submit" class="btn btn-primary">
    </div>
  </div>

</form>
<hr>

<?php
// send to the api
  if (isset($_POST[Config::TYPE_OF_DATA])) {
    $curlPostData = [Config::TYPE_OF_DATA => $_POST[Config::TYPE_OF_DATA]];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $configuration->getApiPath());
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPostData);
    

    $response = curl_exec($ch);

//    $result = json_decode($response);

    print_r($response);

  }



?>
</div>
</body>
</html>
