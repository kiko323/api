<?php
  
  class Config
  {
    CONST API_FILE_PATH = 'http://localhost:9001/api/api.php';
    CONST TYPE_OF_DATA = 'month';
    
    public static function getTypeOfData() {
      return self::TYPE_OF_DATA;
    }
    
    public function getApiPath()
    {
      return self::API_FILE_PATH;
    }
    
    public function dataForSelect()
    {
      $months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
      return $months;
    }
    
    
  }
