<?php
  header("Content-Type:application/json");
  
  require_once '../FetchData.php';
  
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST[Config::TYPE_OF_DATA])) {
      
      $dataFromPost = $_POST[Config::TYPE_OF_DATA];
      $dataForExpose = filterDataForExpose($dataFromPost);
      
      apiExpose($dataForExpose);
      
    } else {
      apiExpose(null, true , 'Errors in POST request');
    }
  } else {
    apiExpose(null, 405, 'Method not allowed');
  }
  
  function filterDataForExpose($dataFromPostReq)
  {
    $data = new FetchData();
    $data = $data->prepareForApi();
    
    foreach ($data as $key => $value) {
      if ($key == $dataFromPostReq) {
        $arrOfDataForExpose = $value;
        return $arrOfDataForExpose;
      }
    }
  }
  
  function apiExpose($data = [], $error = false, $message = '')
  {
    $response['error'] = $error;
    $response['message'] = $message;
    $response['data'] = $data;
    
    echo json_encode($response);
    
  }
